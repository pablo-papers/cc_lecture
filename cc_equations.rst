The Coupled Cluster equations
=============================


The exponential ansatz
----------------------

The size-extensivity problem of truncated CI methods can be linked to the
linear parametrization of the CI wave-function. 
The CC wave-function, on the other hand relies on a exponential
parametrization, which introduces the non-linearity required to have 
size-extensive energies (see appendix :ref:`appendix_h2_dimer`).
The CC wave-function is usually written as

.. math::
   | CC \rangle = \exp (\hat{T}) | \Phi_0 \rangle

where the cluster operator :math:`\hat{T}` is given by

.. math::
   \hat{T} = 1 + \hat{T}_1 + \hat{T}_2 \dots \hat{T}_N

:math:`N` is the total number of electrons and each cluster operator :math:`\hat{T}_{\mu}`
produces all the determinants that differ in orbital occupation by :math:`\mu` electrons 
with respect to a reference determinant (e.g. HF). We refer to this process
as (virtual) excitations since it **excites** or promotes :math:`\mu` electrons from
spin-orbitals that are occupied (:math:`i, j...`) in the reference determinant to virtual (unoccupied)
spin-orbitals (:math:`a, b...`).
For example, the singles and doubles operators acting on a reference
determinant lead to

.. math::
   \hat{T}_1 | \Phi_0 \rangle = \sum_i^{occ} \sum_a^{vir} t_i^a | \Phi_i^a \rangle

.. math::
   \hat{T}_2 | \Phi_0 \rangle = \sum_{i<j}^{occ} \sum_{a<b}^{vir} t_{ij}^{ab} | \Phi_{ij}^{ab} \rangle

where the :math:`t` coefficients are usually called the CC amplitudes and are
the equivalent of the expansion coefficients of the CI wave-function, i.e.,
they are the CC wave-function parameters.

.. note::
   The mathematical formalism behind the (virtual) excitation processes is
   called second-quantization. It is at the basis of quantum field theory and
   extensively used in the development of CC theory. For more details on the
   usage of second-quantization in quantum chemistry, see [Helgaker2000]_

The CC exponential ansatz can then be expanded explicitly

.. math::
   \exp(\hat{T}) | \Phi_0 \rangle = (1 + \hat{T} + \frac{1}{2!} \hat{T}^2
   + \frac{1}{3!} \hat{T}^3 + \cdots ) | \Phi_0 \rangle.

This expansion reveals the non-linearity of the parametrization.
We see for examples that even if the cluster operator is truncated to include
only double excitations, higher excitation will remain in the expansion through
the non-linear terms like :math:`\hat{T}_2 \hat{T}_2` which correspond to a
quadruple excitation. 

In CC theory, those terms are the key to size-extensivity. They are also
responsible for the fast convergence of CC models to the FCI limit, since 
for an equivalent number of parameters (e.g. between truncated CI and CC) the
CC expansion will include higher excitations indirectly through those
non-linear terms.


The CC Schrödinger equation
---------------------------

Now that we have seen the expression of the CC wave-function we can insert it into
the electronic Schrödinger equation to determine the CC ground-state energy.

.. math::
   H | CC \rangle = E_{CC} | CC \rangle

For reasons that we will not get into here, it is not possible to have an
efficient formulation of CC theory by relying on the variational principle to
minimize the energy. Instead, the CC Schrödinger equation is projected against
the reference state :math:`\Phi_0`. It is also common to work with a similarity
transformed Hamiltonian,

.. math::
   H^{\hat{T}} =  \exp(-\hat{T}) H  \exp(\hat{T})

which can be shown to preserve the eigenvalue spectrum of the original
Hamiltonian but it is not a Hermitian operator anymore.
We now have

.. math::
   \exp(-\hat{T}) H  | CC \rangle &= \exp(-\hat{T})  E_{CC} | CC \rangle \\
   \exp(-\hat{T}) H  \exp(\hat{T}) | \Phi_0 \rangle &= E_{CC} \exp(-\hat{T}) \exp(\hat{T}) | \Phi_0 \rangle \\
    H^{\hat{T}} | \Phi_0 \rangle  &= E_{CC} | \Phi_0 \rangle.

Projection against the reference determinant leads to the CC energy,

.. math::
   E_{CC} = \langle \Phi_0 | H^{\hat{T}} | \Phi_0 \rangle

or more explicitly

.. math::
   E_{CC} = \langle \Phi_0 | \exp(-\hat{T}) H  \exp(\hat{T}) | \Phi_0 \rangle.

By expanding the exponentials and using the algebra of the second-quantization 
operators (hidden in the cluster operators), one arrives at the following explicit
expression

.. math::
   E_{CC} = E_{\Phi_0} + \sum_i^{occ} \sum_a^{vir} t_i^a \langle \Phi_0| H | \Phi_i^a \rangle 
   + \sum_{i<j}^{occ} \sum_{a<b}^{vir} (t_{ij}^{ab} + t_i^a t_j^b - t_i^b t_j^a )
     \langle \Phi_0| H | \Phi_{ij}^{ab} \rangle

where :math:`E_0` is the energy corresponding to the reference wave-function
:math:`\Phi_0`. When the reference wave-function is the HF determinant the
expression for the CC energy simplifies to 

.. math::
   E_{CC} = E_{HF}  + \sum_{i<j}^{occ} \sum_{a<b}^{vir} (t_{ij}^{ab} + t_i^a t_j^b - t_i^b t_j^a )
      (ia||jb)

due to Brillouin's theorem.

The CC amplitudes
-----------------

From the last equation we see that the CC energy if (of course) depending on
the CC wave-function parameters (the amplitudes). As stated previously these
are not obtained variationally but by projection techniques.
To determine the amplitudes we project the CC Schrödinger equation against the
set of excited determinants (up to the truncation level), i.e., for a coupled
cluster with single and double excitations (CCSD) we have to solve the
following non-linear equations

.. math::
   \langle \Phi_i^a | \exp(-\hat{T}) H  \exp(\hat{T}) | \Phi_0 \rangle &= 0, \\
   \langle \Phi_{ij}^{ab} | \exp(-\hat{T}) H  \exp(\hat{T}) | \Phi_0 \rangle &= 0.

For higher truncation levels we need to project over excited determinants of
higher excitation ranks.
Note that the right-hand-side of the equation is zero due to orthogonality
between the reference determinant and any excited determinant.

The left-hand-side of the equation can be derived into more explicit
expressions, i.e, in terms of amplitudes and integrals (like the final expression
for the energy) which can be implemented into computer programs.

The CC amplitude equations form a set of non-linear equations which have to be
solved iteratively before the CC energy can be calculated.

.. note:: 
   The equations presented here correspond to standard CC theory for which the
   reference wave-function must be a single slater determinant. In general this
   determinant is chosen to be the HF wave-function.


