|Documentation Status|

.. |Documentation Status| image:: https://readthedocs.org/projects/cc_lecture/badge/?version=latest
   :target: http://cc_lecture.readthedocs.io/?badge=latest

Introduction to Coupled Cluster theory
======================================


These notes have been prepared in October 2018 for a two hour lecture on
coupled cluster theory in the context of the course of Prof. Ursula
Röthlisberger entitled *Introduction to Electronic Structure Methods*
at EPFL, Lausanne, Switzerland.

See :ref:`biblio` for the content I used to write these notes and look
also at the :ref:`abbrev` if needed.

Feel free to re-use this documentation for academic purposes and :ref:`contact` me if
you have any question or comment.

.. figure:: _static/quantum_chemistry_blackboard.png

.. toctree::
   :maxdepth: 2
   :numbered:
   :caption: Sections

   recap
   cc_description
   size_extensivity
   cc_equations
   cc_models


.. toctree::
   :maxdepth: 2
   :caption: Appendices

   h2_dimer
   info
