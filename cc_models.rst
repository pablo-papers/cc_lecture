CC models and comments
======================

Standard CC models
------------------

With the equations that we have presented in the previous section, the standard
models of CC theory can be easily defined. Basically a given CC model is
defined by the truncation level of the cluster operator  (a bit like in the CI
models). If only single excitations are included, :math:`\hat{T} = \hat{T}_1`,
the CC energy is equal to the HF energy and this model, denoted CCS, is not
relevant here (it can however be used to calculate excited state properties).
The next model is the most common in the standard CC hierarchy. It includes
single and double excitations and it is denoted CCSD. 

As you can imagine, the next steps consist in including
triples, CCSDT, and quadruples, CCSDTQ, etc.
This allows for a systematic convergence to the FCI solution which is the main
selling point of CC theory (together with the size-extensivity property
described before).

Computational cost
------------------

However, as we get closer to the FCI solution, the computational cost of
solving the CC amplitude equations increases drastically. In particular the
scaling of the method with the size of the system, *N*, goes as follows ,

* CCSD already scales as :math:`N^6`,

* CCSDT scales as :math:`N^8`,

* and CCSDTQ as :math:`N^{10}`...

.. figure:: _static/water.png

   To be more concrete, if it takes 1 minute to perform a CCSD calculation on 1
   water molecule, it would take **120 years** to perform the same calculation on
   a cluster of 20 water molecules.

A lot of work has been done (and is still going on) to lower the computational 
cost of CC models and to develop intermediates in the CC hierarchy.

One particularly popular and successful method is the CCSD(T) model in which,
a standard CCSD calculation is performed and corrected by the effect of triples 
which are included in a perturbative manner (i.e., by relying on arguments from 
many-body perturbation theory).
This method scales as :math:`N^7` with the system size but recover most of the
effects of a full CCSDT calculation.
Due to the success of the CCSD(T) model, it is often described as the gold-standard
of quantum chemistry.

What is CC theory used for?
---------------------------

As we have seen CC theory has the following properties:

* **Fast and systematic convergence** to the FCI solutions.

* **Size-extensivity** of the energy, (wave-function and energy strictly separable).

* Fast increase of the computational scaling with the truncation level, i.e.,
  **high computational cost**.

* **Single-reference** method. CC cannot be applied when more than one electronic 
  configuration is important to describe a system, e.g. when breaking bonds. 
  In other words, the (HF) reference needs to provide a qualitatively good 
  description of the system under consideration.

So what can it be used for? 

The ground-state CC theory that has been introduced here can be used to
calculate very accurate ground-state energies of small molecules near their
equilibrium geometry. 

In addition molecular properties (dipole moments, polarizabilities...) and excitation 
energies can be calculated by combining CC theory with response theory (see also the
equation-of-motion EOM-CC methods).

Being able to calculate energies and properties accurately and with a
systematic convergence to the FCI solutions makes it possible to benchmark more
affordable models (like DFT) that can be applied to larger molecules.

It should also be mentioned that since the late 90s, a lot of effort has been put 
in the development of more affordable CC models and a lot of progress has been made.

