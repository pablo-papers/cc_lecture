.. _appendix_h2_dimer:

CI and CC for the hydrogen dimer
================================

Let us consider two non-interacting hydrogen molecules as an example to
evaluate the strict separability criterion introduced before.
Let us label one molecule **A** and the other **B**.

We assume the following:

* The Hamiltonian is separable,

.. math::
   H_{AB} = H_A + H_B

* The HF wave-function is strictly separable (see [Helgaker2000]_ for the
  demonstration)

.. math::
   | \Phi^{AB}_{HF} \rangle = | \Phi^{A}_{HF} \rangle | \Phi^{B}_{HF} \rangle

and

.. math::
   E^{AB}_{HF} = E^{A}_{HF} + E^{B}_{HF}

Our goal is to show (or rather to illustrate) that truncated CI models  
do not satisfy the same properties, while truncated CC models do.

In order to do that we will take into account only double excitations
(CID and CCD) and use the fact that the two fragments are non
interacting to write the CID and CCD wave-functions in terms of 
double excitations localized on each fragment,

.. math::
   | \Psi^{AB}_{CID}\rangle = (1 + \hat{C}_2^A + \hat{C}_2^B )| \Phi^{AB}_{HF} \rangle 

.. math::
   | \Psi^{AB}_{CCD}\rangle = \exp (\hat{T}_2^A + \hat{T}_2^B )| \Phi^{AB}_{HF} \rangle 

Where :math:`\hat{C}_2^X` and :math:`\hat{T}_2^X` are equivalent operators
producing all possible doubly excited determinants on fragment :math:`X`.
In a minimal basis for the hydrogen molecule this produces only one determinant
with the corresponding coefficient :math:`c_2^X` or amplitude :math:`t_2^X`

.. math::
   \hat{T}_2^X | \Phi_{HF}^X \rangle = t_{2}^{X} | \Phi_{2}^{X} \rangle

.. math::
   \hat{C}_2^X | \Phi_{HF}^X \rangle = c_{2}^{X} | \Phi_{2}^{X} \rangle

where :math:`| \Phi_{2}^{X} \rangle` is the only accessible doubly excited
determinant on a hydrogen molecule fragment :math:`X` in a minimal basis.

For example, one term in the CID expansion will be written as
   
.. math::
   \hat{C}_2^A | \Phi_{HF}^{AB} \rangle &= \hat{C}_2^A | \Phi_{HF}^{A} \rangle  | \Phi_{HF}^{B} \rangle \\
   &= c_{2}^{A} | \Phi_{2}^{A} \rangle | \Phi_{HF}^{B} \rangle

We now have all the tools to show that in the case of the hydrogen dimer in a
minimal basis the CID model is not strictly separable, while the CCD model is.

This is left as an exercise... ;)

.. note::
   The key is that in the CID expansion for the dimer, the term where both
   fragments are excited will be missing, :math:`c_2^A c_2^B | \Phi_2^A \rangle | \Phi_2^B \rangle`.
   This term is responsible for the lack of size-extensivity of truncated CI.
   However it is present in the CC expansion due to the non-linearity of the
   parametrization.
   
