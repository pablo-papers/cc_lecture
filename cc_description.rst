What is Coupled Cluster Theory?
===============================

Coupled Cluster Theory corresponds to a set of models that, similarly to
truncated CI and MP theory, have been designed as **post-HF** methods targeting the
FCI solution to the electronic Schrödinger equation. 

The goal of CC is thus to recover the correlation energy, defined as the
difference between the FCI and the HF energy in a given basis set. And to do
that as efficiently as possible, i.e., to recover most of the correlation
energy at the minimum computational cost.

CC theory is often preferred over the CI and MP methods for the two following
reasons: 

* It is **size-extensive** (we will clarify that point in the next section),

* and it provides **fast and systematic convergence** to the FCI solutions.

Perturbation models like MP theory often have convergence troubles, and,
as we will see in the next section, truncated CI methods do not provide
size-extensive energies.

However, these properties of CC theory come at the expense of a variational
formulation. So in standard CC theory, **the CC energy is not variational**.
Another major issue for CC theory is its **high computational cost** and in 
particular the scaling of the cost with the size of the system.


