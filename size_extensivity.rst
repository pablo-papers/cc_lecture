Size-consistency, size-extensivity, and strict separability
===========================================================

Those three concepts are used to qualify and differentiate quantum chemistry models 
from one another. 
A lot of confusion exists in the literature and it is thus important to explain
what is meant when using those words.

Size-consistency
----------------

The first concept, that we will refer to as *size-consistency*, has to do with 
the ability of a model to properly describe the entire potential energy surface
of a system.  For example at the equilibrium geometry but also when all the
elements are far apart as well as the intermediate regions (bond-breaking
processes). The size-consistency concept (as defined here) is thus more of a 
system dependent problem than a property of an approximate quantum chemistry
model. Of course the FCI solutions are always size-consistent. However, HF
is not always size-consistent (e.g. hydrogen dissociation) and thus all approximate 
post-HF methods will inherit from this issue. When HF is failing, one should 
instead consider multi-configuration and multi-reference methods to provide a 
size-consistent description of the system under consideration.

.. note:: 
   Unfortunately, the term size-consistent is also often used to describe the
   concept explained below (size-extensivity, and strict separability) which
   are related but fondamentally different.
   This leads to a lot of confusion...

Size-extensivity
----------------

This term was introduced in analogy to size-extensive properties in
thermodynamics. A quantum chemistry model (like CC, and FCI) should provide 
size-extensive energies in the sense that the energy should grow linearly with 
the number of electrons in the system. 

Strict separability
-------------------

This is the easiest concept to define clearly and the one we are really interested in here.

The idea is that if a system is composed of non-interacting fragments A and B,
such that

.. math::
    H^{AB} | \Psi^{AB} \rangle = E^{AB} | \Psi^{AB} \rangle

.. math::
    H^{A} | \Psi^{A} \rangle = E^{A} | \Psi^{A} \rangle, 
    \qquad \text{and} \qquad 
    H^{B} | \Psi^{B} \rangle = E^{B} | \Psi^{B} \rangle,

and such that **we can split the Hamiltonian** of the total system as

.. math::
   H^{AB} = H^A + H^B,

then, a wave-function model that is *strictly separable* will fulfill the
following conditions on the resulting wave-function and energy.

.. math::
   | \Psi^{AB} \rangle = | \Psi^{A} \rangle | \Psi^{B} \rangle
    \qquad \text{and} \qquad 
   E^{AB} = E^{A} + E^{B}

The key here is to understand that when talking about non-interacting system we
mean that the total Hamiltonian can be decomposed into the sum of the
Hamiltonian of the fragments.

.. warning::
   If one is to compute the CC energy of, e.g., a water molecule for a very large
   H--O bond length (e.g. 20 Angstroms), 
   the relation above does not hold unless we specify excplicitly to the computer 
   program that the Hamiltonian should be written as two independent contributions.
   Otherwise, the CC energy of the total system will suffers from the lack of 
   *size-consitency* of the HF solution. CC energies are therefore not always 
   size-consistent (as defined above).

   The size-extensivity and strict separability properties described here are
   mathematical properties and one should be careful when testing those properties
   numerically.


Why do we want size-extensivity?
--------------------------------

So why is it important for a good quantum chemistry model to satisfy the strict
separability and the size-extensivity conditions above?

The first and most important reason is that those properties are
properties of the exact solution to the electronic Schrödinger equation. 
For the other reasons, I cannot put it better than that:

   "An important advantage of a size-extensive method is that it allows straightforward 
   comparisons between calculations involving variable numbers of electrons, e.g. 
   ionization processes or calculations using different numbers of active electrons. 
   Lack of size-extensivity implies that errors from the exact energy increase as more 
   electrons enter the calculation." 

*Taken from:* `"Size-Extensivity and Size-Consistency" <https://web.archive.org/web/20170606215414/http://www.uam.es/docencia/quimcursos/Docs/Knowledge/Fundamental_Theory/cc/node7.html>`_
(1996) by T. Daniel Crawford. 

One of the main troubles of truncated CI models is that they do not satisfy the
strict separability and size-extensivity properties. On the other hand, truncated
CC methods do satisfy those properties. 
In the appendix :ref:`appendix_h2_dimer` we propose an illustration of the
strict separability problem for a dimer of hydrogen in a minimal basis. 

.. note::
   HF and MP theories also satisfy the strict separability and size-extensivity requirements. 

