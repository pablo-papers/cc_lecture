.. _abbrev:

List of abbreviations
---------------------

========  ========================================
CI:       configuration interaction (theory)
CC:       coupled cluster (theory)
CCS:      CC with only single excitations (model)
CCSD:     CC with singles and doubles (model)
CCSD(T):  CCSD and perturbative triples (model)
CCSDT:    CCSD and full triples (model)
CCSDTQ:   CCSDT and full quadruples (model)
DFT:      density functional theory
EOM-CC:   equation of motion CC (theory)
FCI:      full CI (model)
HF:       Hartree--Fock (theory)
MO:       molecular orbital
MP:       Møller--Plesset (perturbation theory)
========  ========================================

.. _biblio:

References
----------

This notes are based on the following literature.
The original lecture was part of Ref. [Roethlisberger2018]_.
A very nice (and more complete) introduction to CC theory is given in Ref.
[Jensen2007]_ without relying on the second-quantization formalism.

However, to go a bit further in the CC equations the second-quantization
formalism cannot be avoided. Two different *schools* exist to deal with this
formalism. The normal-ordering and diagrammatic representation nicely
introduced in [Crawford2007]_ and the commutator based formalism developed in
[Helgaker2000]_.

.. [Roethlisberger2018] U. Röthlisberger, (2018)
   Introduction to Electronic Structure Methods.
   *Lecture notes*, Chapter 7, page 75.

.. [Jensen2007] F. Jensen, (2007). 
   Introduction to Computational Chemistry (Second Edition). 
   *Chichester, UK: John Wiley & Sons, Ltd.* , Section 4.9, page 169.
   ISBN: 978-0-470-01187-4

.. [Crawford2007] D. T. Crawford, and H. F. Schaefer III, (2007).
   An Introduction to Coupled Cluster Theory for Computational Chemists.
   *Reviews in Computational Chemistry*, pages 33-136.
   https://doi.org/10.1002/9780470125915.ch2

.. [Helgaker2000] T. Helgaker, P. Jørgensen, and J. Olsen, (2000). 
   Molecular Electronic-Structure Theory (First Edition). 
   *Chichester, UK: John Wiley & Sons, Ltd.*, Chapters 4,5,13.
   http://doi.org/10.1002/9781119019572

.. _contact:

Contact
-------

* Pablo Baudin
* Scientific collaborator at `LCBC EPFL <https://lcbc.epfl.ch>`_.
* `pablo.baudin@epfl.ch <pablo.baudin@epfl.ch>`_

..
   Indices and tables
   ==================
..
   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`

